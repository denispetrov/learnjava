package lesson4.part2;

import lesson4.exception.NotNumberException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestMathBox {
    private final List<Integer> input = new ArrayList<>();

    {
        input.add(10);
        input.add(1);
        input.add(100000);
        input.add(0);
        input.add(-10);
    }

    @Test
    public void summatorPositiveTest() throws NotNumberException {
        MathBox objectBox = new MathBox(input);
        assertEquals(100001, objectBox.summator());
    }

    @Test
    public void splitterPositiveTest() throws NotNumberException {
        MathBox objectBox = new MathBox(input);
        List<Integer> result = Arrays.asList(1, 0, 10000, 0, -1);
        assertEquals(result, objectBox.splitter(10));
    }

    @Test
    public void addAndRemoveObjectTest() throws NotNumberException {
        MathBox objectBox = new MathBox(input);
        objectBox.addObject(2356);
        objectBox.addObject(-123);
        assertEquals(102234, objectBox.summator());
        objectBox.deleteObject(-123);
        assertEquals(102357, objectBox.summator());
    }
}
