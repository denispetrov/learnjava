package lesson4.part1;

import lesson4.exception.NotNumberException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestObjectBox {
    private final List<Object> input = new ArrayList<>();

    {
        input.add(10);
        input.add(1.2);
        input.add(100000L);
        input.add(0);
        input.add(-10);
    }

    @Test
    public void summatorPositiveTest() throws NotNumberException {
        ObjectBox objectBox = new ObjectBox(input);
        assertEquals(100001, objectBox.summator());
    }

    @Test(expected = NotNumberException.class)
    public void summatorNegativeTest() throws NotNumberException {
        ObjectBox objectBox = new ObjectBox(input);
        objectBox.addObject("BOOOOOOM!!!");
        objectBox.summator();
    }

    @Test
    public void splitterPositiveTest() throws NotNumberException {
        ObjectBox objectBox = new ObjectBox(input);
        List<Integer> result = Arrays.asList(1, 0, 10000, 0, -1);
        assertEquals(result, objectBox.splitter(10));
    }

    @Test(expected = NotNumberException.class)
    public void splitterNegativeTest() throws NotNumberException {
        ObjectBox objectBox = new ObjectBox(input);
        objectBox.addObject("BOOOOOOM!!!");
        objectBox.splitter(10);
    }

    @Test
    public void addAndRemoveObjectTest() throws NotNumberException {
        ObjectBox objectBox = new ObjectBox(input);
        objectBox.addObject(2356);
        objectBox.addObject(-123);
        assertEquals(102234, objectBox.summator());
        objectBox.deleteObject(-123);
        assertEquals(102357, objectBox.summator());
    }
}
