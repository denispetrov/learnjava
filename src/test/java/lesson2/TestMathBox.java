package lesson2;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMathBox {
    MathBox mathBox;

    @Before
    public void before() {
        mathBox = new MathBox();
    }

    @Test
    public void summaTestsPositivePlusPositive() {
        assertEquals(2, mathBox.summa(1, 1));
        assertEquals(-2, mathBox.summa(Integer.MAX_VALUE, Integer.MAX_VALUE));
        assertEquals(30, mathBox.summa(25, 5));
        assertEquals(30, mathBox.summa(5, 25));
    }

    @Test
    public void summaTestsNegativePlusNegative() {
        assertEquals(-2, mathBox.summa(-1, -1));
        assertEquals(0, mathBox.summa(Integer.MIN_VALUE, Integer.MIN_VALUE));
        assertEquals(-30, mathBox.summa(-25, -5));
        assertEquals(-30, mathBox.summa(-5, -25));
    }

    @Test
    public void summaTestsNegativePlusPositive() {
        assertEquals(0, mathBox.summa(1, -1));
        assertEquals(-1, mathBox.summa(Integer.MIN_VALUE, Integer.MAX_VALUE));
        assertEquals(-20, mathBox.summa(-25, 5));
        assertEquals(-20, mathBox.summa(5, -25));
    }

    @Test
    public void summaTestsZero() {
        assertEquals(1, mathBox.summa(1, 0));
        assertEquals(-1, mathBox.summa(-1, 0));
        assertEquals(1, mathBox.summa(0, 1));
        assertEquals(-1, mathBox.summa(0, -1));
        assertEquals(Integer.MIN_VALUE, mathBox.summa(Integer.MIN_VALUE, 0));
        assertEquals(Integer.MAX_VALUE, mathBox.summa(Integer.MAX_VALUE, 0));
        assertEquals(25, mathBox.summa(25, 0));
        assertEquals(-25, mathBox.summa(-25, 0));
        assertEquals(25, mathBox.summa(0, 25));
        assertEquals(-25, mathBox.summa(0, -25));
    }

    @Test
    public void diffTestsResultZero() {
        assertEquals(0, mathBox.diff(0, 0));
        assertEquals(0, mathBox.diff(1, 1));
        assertEquals(0, mathBox.diff(-1, -1));
        assertEquals(0, mathBox.diff(Integer.MAX_VALUE, Integer.MAX_VALUE));
        assertEquals(0, mathBox.diff(Integer.MIN_VALUE, Integer.MIN_VALUE));
        assertEquals(0, mathBox.diff(10, 10));
        assertEquals(0, mathBox.diff(-10, -10));
    }

    @Test
    public void diffTestsResultPositive() {
        assertEquals(Integer.MAX_VALUE, mathBox.diff(Integer.MAX_VALUE, 0));
        assertEquals(Integer.MAX_VALUE, mathBox.diff(0, Integer.MAX_VALUE));
        assertEquals(Integer.MAX_VALUE, mathBox.diff(Integer.MIN_VALUE + 1, 0));
        assertEquals(Integer.MAX_VALUE, mathBox.diff(0, Integer.MIN_VALUE + 1));
        assertEquals(20, mathBox.diff(10, -10));
        assertEquals(20, mathBox.diff(-10, 10));
        assertEquals(2, mathBox.diff(-1, 1));
        assertEquals(2, mathBox.diff(-1, 1));
        assertEquals(1, mathBox.diff(-1, 0));
        assertEquals(1, mathBox.diff(0, 1));
        assertEquals(1, mathBox.diff(1, 0));
        assertEquals(1, mathBox.diff(0, -1));
    }


    @Test(expected = FactorialNegativeInputNumberException.class)
    public void factorialTestsNegative() throws FactorialNegativeInputNumberException {
        mathBox.factorial(-1);
    }

    @Test
    public void factorialTestsPositive() throws FactorialNegativeInputNumberException {
        assertEquals(1, mathBox.factorial(0));
        assertEquals(1, mathBox.factorial(1));
        assertEquals(2, mathBox.factorial(2));
        assertEquals(120, mathBox.factorial(5));
        assertEquals(87178291200L, mathBox.factorial(14));
    }

    @Test(expected = StackOverflowError.class)
    public void factorialTestsWithExceptionStackOverflowError() {
        mathBox.factorialWithError(5);
    }

    @Test
    public void dividerInsideTestsZero() {
        assertEquals(0, mathBox.dividerExceptionInside(10, 0), 0.1);
    }

    @Test(expected = DivideByZeroException.class)
    public void dividerTestsException() throws DivideByZeroException {
        mathBox.divider(1, 0);
    }

    @Test
    public void dividerTest() throws DivideByZeroException {
        assertEquals(2, mathBox.divider(6, 3), 0);
    }
}
