package lesson3;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestMathBox {

    MathBox mathBox;


    @Test
    public void summatorTests() {
        mathBox = new MathBox(new Integer[]{1, 2, 3, 5, 6});
        assertEquals(17, mathBox.summator());
    }

    @Test
    public void splitterTests() {
        mathBox = new MathBox(new Integer[]{2, 4, 8, 16, 32});
        List<Integer> result = Arrays.asList(1, 2, 4, 8, 16);
        assertEquals(result, mathBox.splitter(2));
    }

    @Test
    public void removeNumberInListTests() {
        mathBox = new MathBox(new Integer[]{2, 4, 8, 16, 32});
        List<Integer> result = Arrays.asList(2, 4, 8, 16);
        mathBox.removeNumberInList(32);
        assertEquals(result, mathBox.getList());
    }
}
