package lesson5;

import lesson5.exception.UserNotFoundException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class EmployeeFileManagerTests {

    private final Employee employee1 = new Employee("test1", 23, 3000, "job");
    private final Employee employee2 = new Employee("test2", 24, 6000, "job2");
    private final Employee employee3 = new Employee("test3", 26, 8000, "job2");
    private final Employee employee2newJob = new Employee("test2", 24, 6000, "job5");
    private final Employee employee3newJob = new Employee("test3", 26, 8000, "job5");
    private final Employee employee3ChangedSalaryAndJob = new Employee("test3", 26, 10000, "job3");


    @Test
    public void saveTests() throws Exception {
        EmployeeFileManager employeeFileManager = new EmployeeFileManager();
        employeeFileManager.save(employee1);
        employeeFileManager.save(employee2);
        employeeFileManager.save(employee3);

        List<Employee> expectedList = new ArrayList<>();
        expectedList.add(employee1);
        expectedList.add(employee2);
        expectedList.add(employee3);

        assertEquals(expectedList, employeeFileManager.readList());
    }

    @Test
    public void saveNegativeTests() throws Exception {
        EmployeeFileManager employeeFileManager = new EmployeeFileManager();
        employeeFileManager.save(employee3);
        assertFalse(employeeFileManager.save(employee3));
    }

    @Test
    public void saveOrUpdateTests() throws Exception {
        EmployeeFileManager employeeFileManager = new EmployeeFileManager();
        employeeFileManager.save(employee1);
        employeeFileManager.save(employee2);
        employeeFileManager.save(employee3);
        employeeFileManager.saveOrUpdate(employee3ChangedSalaryAndJob);

        List<Employee> expectedList = new ArrayList<>();
        expectedList.add(employee1);
        expectedList.add(employee2);
        expectedList.add(employee3ChangedSalaryAndJob);

        assertEquals(expectedList, employeeFileManager.readList());
    }

    @Test
    public void deleteTests() throws Exception {
        EmployeeFileManager employeeFileManager = new EmployeeFileManager();
        employeeFileManager.save(employee1);
        employeeFileManager.save(employee2);
        employeeFileManager.save(employee3);
        employeeFileManager.delete(employee3);

        List<Employee> expectedList = new ArrayList<>();
        expectedList.add(employee1);
        expectedList.add(employee2);

        assertEquals(expectedList, employeeFileManager.readList());
    }

    @Test
    public void getByNameTests() throws Exception {
        EmployeeFileManager employeeFileManager = new EmployeeFileManager();
        employeeFileManager.save(employee1);
        employeeFileManager.save(employee2);
        employeeFileManager.save(employee3);

        assertEquals(employee3, employeeFileManager.getByName(employee3.getName()));
    }

    @Test(expected = UserNotFoundException.class)
    public void getByNameNegativeTests() throws Exception {
        EmployeeFileManager employeeFileManager = new EmployeeFileManager();
        employeeFileManager.save(employee1);
        employeeFileManager.save(employee2);
        employeeFileManager.save(employee3);

        assertEquals(employee3, employeeFileManager.getByName("employee3.getName()"));
    }

    @Test
    public void getByJobTests() throws Exception {
        EmployeeFileManager employeeFileManager = new EmployeeFileManager();
        employeeFileManager.save(employee1);
        employeeFileManager.save(employee2);
        employeeFileManager.save(employee3);

        List<Employee> expectedList = new ArrayList<>();
        expectedList.add(employee2);
        expectedList.add(employee3);

        assertEquals(expectedList, employeeFileManager.getByJob(employee2.getJob()));
    }

    @Test(expected = UserNotFoundException.class)
    public void getByJobNegativeTests() throws Exception {
        EmployeeFileManager employeeFileManager = new EmployeeFileManager();
        employeeFileManager.save(employee1);
        employeeFileManager.save(employee2);
        employeeFileManager.save(employee3);

        List<Employee> expectedList = new ArrayList<>();
        expectedList.add(employee2);
        expectedList.add(employee3);

        assertEquals(expectedList, employeeFileManager.getByJob("employee3.getJob()"));
    }

    @Test
    public void changeAllWorkTests() throws Exception {
        EmployeeFileManager employeeFileManager = new EmployeeFileManager();
        employeeFileManager.save(employee1);
        employeeFileManager.save(employee2);
        employeeFileManager.save(employee3);

        assertTrue(employeeFileManager.changeAllWork(employee2.getJob(), employee2newJob.getJob()));

        List<Employee> expectedList = new ArrayList<>();
        expectedList.add(employee2newJob);
        expectedList.add(employee3newJob);

        assertEquals(expectedList, employeeFileManager.getByJob(employee2newJob.getJob()));
    }

}
