package lesson6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Timer extends Thread {
    private static final Logger logger = LogManager.getLogger(Timer.class);
    private final int count;
    private final Chronometer chronometer;

    public Timer(int count, Chronometer chronometer) {
        this.count = count;
        this.chronometer = chronometer;
    }

    @Override
    public void run() {
        while (true) {
            for (int i = 0; i < count; i++) {
                synchronized (chronometer) {
                    try {
                        chronometer.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            logger.info(String.format("%d times passed", count));
        }
    }
}
