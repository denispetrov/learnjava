package lesson6;

public class Main {
    public static void main(String[] args) {
        Chronometer chronometer = new Chronometer();
        Timer timer1 = new Timer(5, chronometer);
        Timer timer4 = new Timer(7, chronometer);
        timer1.start();
        timer4.start();
        chronometer.start();
    }
}
