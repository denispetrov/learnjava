package lesson6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Chronometer extends Thread {
    private static final Logger logger = LogManager.getLogger(Chronometer.class);
    private int count = 0;

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            logger.info(String.format("%d, one time again", ++count));
            synchronized (this) {
                this.notifyAll();
            }
        }
    }
}
