package lab1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class Finder implements MyInterface {
    private static final Logger logger = LogManager.getLogger(Finder.class);
    private final ExecutorService pool = Executors.newFixedThreadPool(60);
    private final byte[] buffer = new byte[64000];
    private final int count = 0;
    private final BlockingQueue<String> queue = new LinkedBlockingQueue<>();
    private int bufferSize = 64000;

    public static byte[] arraysSum(byte[] arr, byte[] elements) {
        byte[] tempArr = new byte[arr.length + elements.length];
        System.arraycopy(arr, 0, tempArr, 0, arr.length);

        for (int i = 0; i < elements.length; i++)
            tempArr[arr.length + i] = elements[i];
        return tempArr;
    }

    @Override
    public void getOccurrences(String[] sources, String[] words, String res) {
        logger.info("Method: getOccurrences. Start.");
        Thread fileWriter = new Thread(() -> {
            try (PrintWriter writer = new PrintWriter(new File(res))) {
                while (true) {
                    writer.println(queue.take());
                }
            } catch (IOException | InterruptedException e) {
                logger.info("Thread: PrintWriter. Interrupted.");
            }
        });
        fileWriter.start();

        for (String source : sources) {
            String ends = "";
            try (FileInputStream inputStream = new FileInputStream(source)) {
                while (inputStream.available() > 0) {
                    if (inputStream.available() < 64000) bufferSize = inputStream.available();
                    inputStream.read(buffer, 0, bufferSize);
                    if (!ends.isEmpty()) {
                        ends = lookingForSentenceInArrayAndAddThreadsToPool(arraysSum(ends.getBytes(), buffer), bufferSize, words);
                    } else {
                        ends = lookingForSentenceInArrayAndAddThreadsToPool(buffer, bufferSize, words);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        pool.shutdown();
        fileWriter.interrupt();
        logger.info("Method: getOccurrences. End.");
    }

    private String lookingForSentenceInArrayAndAddThreadsToPool(byte[] chars, int len, String[] words) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < len; i++) {
            char symbol = (char) chars[i];
            if (symbol >= 'A' && symbol <= 'Z') {
                stringBuilder.append(symbol);
                for (int j = i + 1; j < len; j++) {
                    symbol = (char) chars[j];
                    stringBuilder.append(symbol);
                    if (symbol == '.' || symbol == '!' || symbol == '?') {
                        pool.execute(new LookingForArraysElementInString(stringBuilder.toString(), words, queue));
                        stringBuilder = new StringBuilder();
                        i = j;
                        break;
                    }
                }
            }
        }
        return stringBuilder.toString();
    }
}
