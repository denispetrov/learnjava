package lab1;

public class SimpleRandomSentences {

    static final private String[] nouns = {"farmer", "rooster", "judge", "man", "maiden",
            "cow", "dog", "cat", "cheese"};

    static final private String[] verbs = {"kept", "waked", "married",
            "milked", "tossed", "chased", "lay in"};

    static final private String[] modifiers = {"that crowed in the morn", "sowing his corn",
            "all shaven and shorn",
            "all forlorn", "with the crumpled horn"};

    public String randomSentence() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(randomSimpleSentence());

        /* Optionally, "and" followed by another simple sentence.*/

        if (Math.random() > 0.75) { // 25% of sentences continue with another clause.
            stringBuilder.append(" and ");
            stringBuilder.append(randomSimpleSentence());
        }
        if (Math.random() > 0.70) { // 30% of sentences ends point.
            stringBuilder.append(".");
        } else if (Math.random() > 0.70) {
            stringBuilder.append("!");
        } else {
            stringBuilder.append("?");
        }

        return stringBuilder.toString();
    }

    private String randomSimpleSentence() {

        StringBuilder stringBuilder = new StringBuilder("This is ");

        if (Math.random() > 0.15) { // 85% of sentences have a noun phrase.
            stringBuilder.append(randomNounPhrase());
        }
        stringBuilder.append("the house that Jack built");
        return stringBuilder.toString();
    }

    private String randomNounPhrase() {
        StringBuilder stringBuilder = new StringBuilder();
        int n = (int) (Math.random() * nouns.length);
        stringBuilder.append("the " + nouns[n]);
        if (Math.random() > 0.75) { // 25% chance of having a modifier.
            int m = (int) (Math.random() * modifiers.length);
            stringBuilder.append(" " + modifiers[m]);
        }
        int v = (int) (Math.random() * verbs.length);
        stringBuilder.append(" that " + verbs[v] + " ");
        if (Math.random() > 0.5) {  // 50% chance of having another noun phrase.
            stringBuilder.append(randomNounPhrase());
        }
        return stringBuilder.toString();
    }
}