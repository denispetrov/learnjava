package lab1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileWriter;
import java.io.IOException;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        String[] sources = {"sources0.txt", "sources1.txt", "sources2.txt", "sources3.txt"};
        String[] words = {"Cat", "Dog"};
        String fileOutputName = "output.txt";
        makeTestFiles(sources, 10000);
        new Finder().getOccurrences(sources, words, fileOutputName);
    }

    private static void makeTestFiles(String[] filenames, int count) {
        SimpleRandomSentences simpleRandomSentences = new SimpleRandomSentences();
        for (String filename : filenames) {
            logger.info(String.format("Generate and write sentences in file %s: Start", filename));
            try (FileWriter fileWriter = new FileWriter(filename)) {
                for (int i = 0; i < count; i++) {
                    fileWriter.write(simpleRandomSentences.randomSentence() + "\r\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            logger.info(String.format("Generate and write sentences in file %s: Stop", filename));
        }
    }
}
