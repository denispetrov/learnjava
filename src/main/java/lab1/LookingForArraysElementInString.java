package lab1;

import java.util.concurrent.BlockingQueue;

public class LookingForArraysElementInString extends Thread {
    private final String[] array;
    private final String string;
    private final BlockingQueue<String> queue;

    public LookingForArraysElementInString(String string, String[] array, BlockingQueue<String> queueInstance) {
        this.string = string;
        this.array = array;
        this.queue = queueInstance;
    }

    @Override
    public void run() {
        for (String s : array) {
            if (string.toUpperCase().contains(s.toUpperCase())) {
                try {
                    queue.put(string);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
