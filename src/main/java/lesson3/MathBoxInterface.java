package lesson3;

import java.util.List;

public interface MathBoxInterface {
    int summator();

    List<Integer> splitter(int divider);

    void removeNumberInList(int a);

    List<Integer> getList();
}
