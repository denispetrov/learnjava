package lesson3;

import java.util.*;

public class MathBox implements MathBoxInterface {

    private final List<Integer> list;

    public MathBox(Integer[] array) {
        list = new ArrayList<>(Arrays.asList(array));
        list.sort(Comparator.naturalOrder());
    }

    @Override
    @ClearData
    @Logged
    public int summator() {
        int summa = 0;
        for (Integer number : list) {
            summa += number;
        }
        return summa;
    }

    @Override
    public List<Integer> splitter(int divider) {
        List<Integer> dividerList = new ArrayList<>();
        for (Integer number : list) {
            dividerList.add(number / divider);
        }
        return dividerList;
    }

    @Override
    public void removeNumberInList(int a) {
        if (list.contains(a)) {
            list.remove(Integer.valueOf(a));
        }
    }

    @Override
    public List<Integer> getList() {
        return list;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MathBox mathBox = (MathBox) o;
        return list.equals(mathBox.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(list);
    }

    @Override
    public String toString() {
        return "MathBox{" +
                "list=" + list +
                '}';
    }
}
