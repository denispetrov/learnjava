package lesson5;

import lesson5.exception.UserNotFoundException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class EmployeeFileManager {

    private final File file = new File("DataBase");

    public EmployeeFileManager() {
        if (file.exists()) file.delete();
    }

    public boolean save(Employee employee) {
        List<Employee> result = readList();

        if (result
                .stream()
                .filter(employee1 -> (employee1.getName().equals(employee.getName())))
                .anyMatch(employee1 -> (employee1.getAge().equals(employee.getAge())))) {
            return false;
        } else {
            result.add(employee);
            writeList(result);
            return true;
        }
    }

    public boolean delete(Employee employee) {
        List<Employee> result = readList();

        if (result.contains(employee)) {
            result.remove(employee);
            writeList(result);
            return true;
        } else {
            return false;
        }
    }

    public Employee getByName(String name) throws UserNotFoundException {
        Optional<Employee> result = readList()
                .stream()
                .filter(employee -> employee.getName().equals(name))
                .findAny();

        if (result.isPresent()) {
            return result.get();
        } else {
            throw new UserNotFoundException("User not found");
        }
    }

    public List<Employee> getByJob(String job) throws UserNotFoundException {
        List<Employee> result = readList()
                .stream()
                .filter(employee -> employee.getJob().equals(job))
                .collect(Collectors.toList());

        if (result.size() > 0) {
            return result;
        } else {
            throw new UserNotFoundException("User not found");
        }
    }

    public boolean saveOrUpdate(Employee employee) {
        List<Employee> result = readList();
        Optional<Employee> employeeForUbldate = result
                .stream()
                .filter(employee1 -> (employee1.getName().equals(employee.getName())))
                .filter(employee1 -> (employee1.getAge().equals(employee.getAge())))
                .findAny();
        employeeForUbldate.ifPresent(result::remove);
        result.add(employee);
        writeList(result);
        return true;
    }

    public boolean changeAllWork(String firstJob, String newJob) {
        List<Employee> list = readList();
        List<Employee> employeesHaveFirstJob = list
                .stream()
                .filter(employee -> employee.getJob().equals(firstJob))
                .collect(Collectors.toList());
        list.removeAll(employeesHaveFirstJob);
        List<Employee> employeesHaveNewJob = employeesHaveFirstJob
                .stream()
                .map(employee -> {
                    employee.setJob(newJob);
                    return employee;
                })
                .collect(Collectors.toList());
        writeList(employeesHaveNewJob);
        return true;
    }

    private void writeList(List<Employee> list) {
        file.deleteOnExit();
        try (ObjectOutputStream objectInputStream = new ObjectOutputStream(new FileOutputStream(file, false))) {
            objectInputStream.writeObject(list);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public List<Employee> readList() {
        List<Employee> result = new ArrayList<>();
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            result = (List<Employee>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return result;
    }
}