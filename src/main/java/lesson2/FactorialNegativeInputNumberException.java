package lesson2;

public class FactorialNegativeInputNumberException extends ArithmeticException {
    public FactorialNegativeInputNumberException(String message) {
        super(message);
    }
}
