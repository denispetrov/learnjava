package lesson2;

public class MathBox {
    public int summa(Integer a, Integer b) {
        return a + b;
    }

    public int diff(Integer a, Integer b) {
        return Math.abs(a - b);
    }

    public long factorial(Integer a) throws FactorialNegativeInputNumberException {
        if (a < 0) throw new FactorialNegativeInputNumberException("Factorial only for positive numbers");
        if (a == 0 || a == 1) return 1;
        return a * factorial(a - 1);
    }

    public long factorialWithError(Integer a) {
        return a * factorialWithError(a - 1);
    }

    public double dividerExceptionInside(Integer a, Integer b) {
        double result = 0;
        try {
            result = a / b;
        } catch (ArithmeticException e) {
            e.printStackTrace();
        }

        return result;
    }

    public double divider(Integer a, Integer b) throws DivideByZeroException {
        if (b == 0) throw new DivideByZeroException("Divide By Zero!");
        return a / b;
    }
}
