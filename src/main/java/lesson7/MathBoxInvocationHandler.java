package lesson7;

import lesson3.ClearData;
import lesson3.Logged;
import lesson3.MathBoxInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class MathBoxInvocationHandler implements InvocationHandler {
    private static final Logger logger = LogManager.getLogger(MathBoxInvocationHandler.class);
    private final MathBoxInterface mathBoxInterface;

    public MathBoxInvocationHandler(MathBoxInterface mathBoxInterface) {
        this.mathBoxInterface = mathBoxInterface;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (mathBoxInterface.getClass().getMethod(method.getName()).getAnnotation(Logged.class) != null) {
            logger.info(String.format("%s method was called", method.getName()));
        }
        if (mathBoxInterface.getClass().getMethod(method.getName()).getAnnotation(ClearData.class) != null) {
            Field enterSet = mathBoxInterface.getClass().getDeclaredField("list");
            enterSet.setAccessible(true);
            enterSet.set(mathBoxInterface, new ArrayList<>());
            logger.info("Data was cleared");
        }
        return method.invoke(mathBoxInterface, args);
    }
}
