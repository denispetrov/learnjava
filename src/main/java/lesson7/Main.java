package lesson7;

import lesson3.MathBox;
import lesson3.MathBoxInterface;

import java.lang.reflect.Proxy;

public class Main {

    public static void main(String[] args) {
        MathBoxInterface mathBoxInterface = new MathBox(new Integer[]{1, 2, 3, 5, 6});
        MathBoxInterface mathBoxInterfaceFix = (MathBoxInterface) Proxy.newProxyInstance(MathBoxInvocationHandler.class.getClassLoader(),
                new Class[]{MathBoxInterface.class},
                new MathBoxInvocationHandler(mathBoxInterface));

        System.out.println(mathBoxInterfaceFix.summator());
    }
}
