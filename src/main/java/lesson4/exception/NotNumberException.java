package lesson4.exception;

public class NotNumberException extends Exception {
    public NotNumberException(String message) {
        super(message);
    }
}
