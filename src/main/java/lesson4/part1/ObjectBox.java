package lesson4.part1;

import lesson4.exception.NotNumberException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ObjectBox {
    private final List<Object> list;

    public ObjectBox(List<Object> list) {
        this.list = list;
    }

    /**
     * Checks that all elements of the inner collection are instances of the Number class
     *
     * @throws NotNumberException if one of the collection's elements is not an instance of the Number class
     */
    private void allElementsAreNumbers() throws NotNumberException {
        for (Object o : list) {
            if (!(o instanceof Number)) {
                throw new NotNumberException("Collection's Element is not Number");
            }
        }
    }

    public int summator() throws NotNumberException {
        allElementsAreNumbers();
        int summa = 0;
        for (Object number : list) {
            summa = summa + ((Number) number).intValue();
        }
        return summa;
    }

    public List<Integer> splitter(int divider) throws NotNumberException {
        allElementsAreNumbers();
        List<Integer> dividerList = new ArrayList<>();
        for (Object number : list) {
            dividerList.add(((Number) number).intValue() / divider);
        }
        return dividerList;
    }

    public void addObject(Object o) {
        list.add(o);
    }

    public void deleteObject(Object o) {
        list.remove(o);
    }

    public void dump() {
        System.out.println("List Objects");
        for (Object number : list) {
            System.out.print(number + " ");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectBox objectBox = (ObjectBox) o;
        return Objects.equals(list, objectBox.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(list);
    }

    @Override
    public String toString() {
        return "ObjectBox{" +
                "list=" + list +
                '}';
    }
}
