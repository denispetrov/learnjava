package lesson4.part2;

import java.util.ArrayList;
import java.util.List;

public class MathBox extends ObjectBox<Integer> {

    public MathBox(List<Integer> list) {
        super(list);
    }

    public int summator() {
        int summa = 0;
        for (Object number : list) {
            summa = summa + ((Number) number).intValue();
        }
        return summa;
    }

    public List<Integer> splitter(int divider) {
        List<Integer> dividerList = new ArrayList<>();
        for (Object number : list) {
            dividerList.add(((Number) number).intValue() / divider);
        }
        return dividerList;
    }
}
