package lesson4.part2;

import java.util.List;
import java.util.Objects;

public class ObjectBox<T> {

    protected final List<T> list;

    public ObjectBox(List<T> list) {
        this.list = list;
    }

    public void addObject(T o) {
        list.add(o);
    }

    public void deleteObject(T o) {
        list.remove(o);
    }

    public void dump() {
        System.out.println("List Objects");
        for (T number : list) {
            System.out.print(number + " ");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectBox objectBox = (ObjectBox) o;
        return Objects.equals(list, objectBox.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(list);
    }

    @Override
    public String toString() {
        return "ObjectBox{" +
                "list=" + list +
                '}';
    }
}
